var _ = require("underscore");
var signalr = require("react-native-signalr").default;

var Exception = require("./Exception");

function Notifier (url, hub) {
    this.url = url;
    this.hub = hub;

    this.connection = {};
    this.proxyHub = {};

    this.subscribers = {};
}
Notifier.prototype.connect = function (token, onSuccess, onError) {
    this.connection = signalr.hubConnection(this.url);
    this.connection.qs = { Bearer: token };

    this.connection.error(function (error) {
        var errorMessage = error.message;
        var detailedError = "";
        if (error.source && error.source._response) {
            detailedError = error.source._response;
        }
        if (detailedError === 'An SSL error has occurred and a secure connection to the server cannot be made.') {
            onError(new Exception("When using react-native-signalr on ios with http remember to enable http in App Transport Security", 0)); // https://github.com/olofd/react-native-signalr/issues/14
        } else {
            // Hotfix: Error on Longpolling connection abort
            if (error.transport === "longPolling" && !error.source._hasError && error.source._aborted)
                return;

            onError(new Exception(errorMessage, 0));
        }
    });

    this.proxyHub = this.connection.createHubProxy(this.hub);

    var self = this;
    this.proxyHub.on("OnEvent", function (notification) {
        if (!_.isUndefined(self.subscribers[notification.EventName])) {
            self.subscribers[notification.EventName](notification);
        }
    });

    this.connection.start()
        .done(function() {
            onSuccess(self.connection.id);
        })
        .fail(function() {
            onError(new Exception("Connection failed", 0)); // TODO: Messages
        });
};
Notifier.prototype.disconnect = function () {
    if (!_.isUndefined(this.connection)) {
        this.connection.stop();
        return true;
    }
    return false;
};
Notifier.prototype.subscribe = function (eventName, callback, force) {
    if (!_.isUndefined(this.proxyHub)) {
        if (_.isUndefined(this.subscribers[eventName]) || force === true) {
            this.proxyHub.invoke("Subscribe", { "EventName": eventName });
            this.subscribers[eventName] = callback;
            return true;
        }
    }
    return false;
};
Notifier.prototype.unsubscribe = function (eventName) {
    if (!_.isUndefined(this.proxyHub)) {
        this.proxyHub.invoke("Unsubscribe", { "EventName": eventName }); // TODO: Not working - server return false
        if (_.has(this.subscribers, eventName)) {
            delete this.subscribers[eventName];
        }
        return true;
    }
    return false;
};

module.exports = Notifier;
